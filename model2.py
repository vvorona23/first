import pandas as pd
from sklearn.linear_model import LinearRegression

df = pd.read_csv('house_pricing.csv')

model = LinearRegression()

model.fit(df[["LotArea"]], df[['SalePrice']])

p = model.predict([[8450]])
print(p)

import pickle

f = open('model.pkl', 'wb')
pickle.dump(model, f)
f.close()