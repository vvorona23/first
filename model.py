f = open('house_pricing.csv', 'r')
content = f.read()
lines = content.split('\n')
lines = lines[1:]

ds = []

for l in lines:
    sl = l.split(',')
    ds.append({
        "LotArea": float(sl[0]),
        "SalePrice": float(sl[1])
    })

prices_per_meter = []
for i in range(len(ds) - 1):
    prices_per_meter.append(ds[i]['SalePrice'] / ds[i]['LotArea'])

k1 = sum(prices_per_meter) / len(prices_per_meter)

print(prices_per_meter)

def model(area):
    return area * k1

prediction0 = model(8450)
print(prediction0)
